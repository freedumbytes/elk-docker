# Elastic and the ELK Stack Docker Demo

## Project Setup

### Git

First create an empty [![Git](./documentation/icons/git.png "Git"){height=16px} Git](https://git-scm.com) project in `%PROJECT_HOME%`:

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
cd /D %PROJECT_HOME%
git init elk-docker
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PowerShell only:

```
cd $env:PROJECT_HOME
git init elk-docker
```

![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash only:

```
cd $PROJECT_HOME
git init elk-docker
```

Configure the [.gitignore](https://gitlab.com/freedumbytes/setup/-/raw/master/.gitignore) and [.gitattributes](https://gitlab.com/freedumbytes/setup/-/raw/master/.gitattributes).

Start this [![Markdown Mark](./documentation/icons/markdown-mark-outline.svg "Markdown Mark"){height=16px}](https://github.com/dcurtis/markdown-mark) `README.md`.

[![Markdown](./documentation/icons/markdown-mark-outline.svg "Markdown"){height=16px} Markdown](https://daringfireball.net/projects/markdown) is a lightweight markup language for creating formatted text using a plain-text editor.

 * [![GitLab](./documentation/icons/gitlab.svg "GitLab"){height=16px} GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html) (GLFM).

### Eclipse

Configure [![Eclipse](./documentation/icons/eclipse.png "Eclipse"){height=16px} Eclipse](https://eclipse.org/downloads/packages) to use this external directory:

 * In Package Explorer right-click and select `New` > `Project…`
 * Select `Wizards` > `General` > `Project` > `Next`:
   * `Project name`: `elk-docker`
   * Uncheck `Use default location`
   * `Location`: `%PROJECT_HOME%\elk-docker`
   * `Working sets` > `New…`
     * `Working set name`: `elk-docker` and click `Finish`
   * Click `Finish` again to create the project.

## WSL2 - Windows Subsystem for Linux

Based on [Windows Subsystem for Linux Documentation](https://learn.microsoft.com/en-us/windows/wsl) and enhanced with some code to verify stuff.

Open an *elevated* [PowerShell](https://learn.microsoft.com/en-us/powershell) (![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS) or [Windows Command Prompt](https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands) (![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD) in **administrator** mode by right-clicking and selecting `Run as administrator`.

To [Install Linux on Windows with WSL](https://learn.microsoft.com/en-us/windows/wsl/install) without looking at the gory details jump to step 6.

Otherwise, follow the [Manual installation steps for older versions of WSL](https://learn.microsoft.com/en-us/windows/wsl/install-manual) below from the start.

### Step 1

Enable the Windows Subsystem for Linux (see also [Enable or Disable Windows Features Using DISM](https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/enable-or-disable-windows-features-using-dism)):

```
dism /Online /Enable-Feature /FeatureName:Microsoft-Windows-Subsystem-Linux /All /NoRestart
dism /Online /Get-FeatureInfo /FeatureName:Microsoft-Windows-Subsystem-Linux
```

**Alternative way**: Check the feature at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Turn Windows features on or off` > `Windows Subsystem for Linux`.

### Step 2

To get the current Windows version, to see if the requirements for WSL 2 are met, use:

```
winver
systeminfo | findstr Version
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
ver
```

### Step 3

Enable Virtual Machine feature:

```
dism /Online /Enable-Feature /FeatureName:VirtualMachinePlatform /All /NoRestart
dism /Online /Get-FeatureInfo /FeatureName:VirtualMachinePlatform
```

**Alternative way**: Check the feature at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Turn Windows features on or off` > `Virtual Machine Platform`.

Restart for both changes from step 1 and 3 to take effect.

Verify at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Task Manager` > `Performance` > `CPU` > `Virtualization`: `Enabled`.

### Step 4

Use `systeminfo` to find out the `System Type`:

```
systeminfo | findstr System
systeminfo | findstr /C:System
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
systeminfo | find "System"
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only (because of issue `FIND: Parameter format not correct`):

```
systeminfo | find """System"""
systeminfo | find `"System`"
systeminfo | find --% "System"
```

Download the Linux kernel update package ([Release Notes](https://learn.microsoft.com/en-us/windows/wsl/release-notes)) and run:

1. [![WSL2](./documentation/icons/wsl2.png "WSL2"){height=16px} WSL2 Linux kernel update X64 package](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
2. [![WSL2](./documentation/icons/wsl2.png "WSL2"){height=16px} WSL2 Linux kernel update ARM64 package](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_arm64.msi)

### Step 5

Set WSL 2 as your default version or for an installed distribution specific (that last one of course after installing it first; see step 6):

```
wsl --set-default-version 2
wsl --set-version Ubuntu-22.04 2
wsl --status
```

### Step 6

Install the default distribution, when you skipped the prior manual steps 1-5, otherwise, this command might just show the WSL help instead:

```
wsl --install
```

Or install your ![Linux](./documentation/icons/linux.png "Linux"){height=16px} Linux distribution of choice, for example ![Ubuntu](./documentation/icons/ubuntu.png "Ubuntu"){height=16px} Ubuntu:

```
wsl --list --online
wsl --install -d Ubuntu-22.04
```

During installation:

```
Installing, this may take a few minutes…
Please create a default UNIX user account. The username does not need to match your Windows username.
For more information visit: https://aka.ms/wslusers
Enter new UNIX username:
```

**Note**: https://aka.ms/wslusers is not redirecting properly to [Set up your Linux username and password](https://learn.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password).

Verify the installation:

```
wsl --status
wsl --list
wsl --list --verbose
wsl -l -v
```

To update WSL, because the prior installed `msi` isn't updated as frequently as the [WSL in the Microsoft Store](https://learn.microsoft.com/en-us/windows/wsl/compare-versions) (in an older WSL version for example `wsl --version` and `wsl --install` were not working as intended):

```
wsl --update
wsl --version
```

To start the default or a specific distribution:

```
wsl
wsl -d Ubuntu-22.04
wsl -d Ubuntu-22.04 --user <unix-username>
```

**Tip**: In case of ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash use `winpty wsl`.

Verify Ubuntu with:

```
whoami
lsb_release -a
cat /etc/os-release
ll /mnt
ll /mnt/c
exit
```

Stop a specific distribution with:

```
wsl -l -v
wsl --terminate Ubuntu-22.04
wsl -l -v
```

Or stop them all with:

```
wsl --shutdown
```

See also [Basic commands for WSL](https://learn.microsoft.com/en-us/windows/wsl/basic-commands).

```
wsl -l -v
wsl date
wsl -l -v
wsl --shutdown
```

## Docker Desktop on Windows

To [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install) download [![Docker Desktop](./documentation/icons/docker-desktop.png "Docker Desktop"){height=16px} Docker Desktop Installer.exe](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe) ([Release Notes](https://docs.docker.com/desktop/release-notes)).

**Known issues**:

 * Versions 4.23.0 - 4.24.2 `Quit Docker Desktop` hangs, causing:
   * ![Docker Failure](./documentation/icons/docker-failure.png "Docker Failure"){height=16px} **Docker failed to initialize** *Docker Desktop is shutting down* when trying to start ![Docker Desktop](./documentation/icons/docker-desktop.png "Docker Desktop"){height=16px} again.
   * ![Docker App](./documentation/icons/wfc.png "Docker App"){height=16px} *This app is preventing shutdown* on ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Power` > `Shut down`.

Verify the cryptographic hash with:

```
certutil -hashfile "Docker Desktop Installer.exe" SHA256
```

The ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash counterpart is:

```
sha256sum Docker\ Desktop\ Installer.exe
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
start /w "" "Docker Desktop Installer.exe" install
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only:

```
Start-Process 'Docker Desktop Installer.exe' -Wait install
```

During installation select the default setting: `Use WSl 2 instead of Hyper-V (recommended)`. The installation logging can be found at:

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
type %LOCALAPPDATA%\Docker\install-log.txt
type %PROGRAMDATA%\DockerDesktop\install-log-admin.txt
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only:

```
type $env:LOCALAPPDATA\Docker\install-log.txt
type $env:PROGRAMDATA\DockerDesktop\install-log-admin.txt
```

**Important**: Make sure to close other applications, because of `docker-users` membership the installation might ask to log out. But if you change your mind and choose `Cancel` in case of the Windows Pop-up about still open applications with the option to go back and save your work, then for example ![Notepad++](./documentation/icons/notepad.png "Notepad++"){height=16px} `Notepad++` will still be open, but all prior open files are gone from the `Document List`.

When the `Installation succeeded` then click `Close and log out`.

After restart run ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Docker Desktop` and `Accept` the `Docker Subscription Service Agreement` after reading.

Use ![Windows Key](./documentation/icons/windows-key.png "Windows Key"){height=16px} + `I` > `Select which icons appear on the taskbar` to add `Docker Desktop`. Use the ![Docker Whale](./documentation/icons/docker-whale.png "Docker Whale"){height=16px} in the system tray to `Go to the Dashboard` > ![Docker Settings](./documentation/icons/docker-settings.png "Docker Settings"){height=16px} `Settings` and take a look at the current settings (for example `General` > `Send usage statistics`) followed by `Apply & retart` Docker Desktop, `Cancel` or ![Close Settings](./documentation/icons/docker-close.png "Close Settings"){height=16px} to return to the Dashboard.

Check available groups and the users in the `docker-users` group and add if necessary:

```
net
net localgroup
net users
net localgroup docker-users
net localgroup docker-users <windows-user> /add
```

**Alternative way**: ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Computer Management` > `System Tools` > `Local Users and Groups` > `Groups` > `docker-users` > `Add…` > `Enter the object names to select` > `Check Names` > `OK`.

[Docker Desktop WSL 2 backend on Windows](https://docs.docker.com/desktop/wsl) installs two special-purpose internal *Linux distros* `docker-desktop` and `docker-desktop-data`. The first (`docker-desktop`) is used to run the Docker engine (`dockerd`) while the second (`docker-desktop-data`) stores containers and images. Neither can be used for general development.

```
wsl -l -v
```

## Apache Lucene MMapDirectory Tuning

When using [![Lucene](./documentation/icons/apache-lucene.png "Lucene"){height=16px} Lucene](https://lucene.apache.org) configure a higher value for `vm.max_map_count` (see also [![PDF](./documentation/icons/pdf.svg "PDF"){height=16px} The future of Lucene's MMapDirectory: Why use it and what's coming with Java 16 and later?](https://2021.berlinbuzzwords.de/sites/berlinbuzzwords.de/files/2021-06/The%20future%20of%20Lucene%27s%20MMapDirectory.pdf) or watch [![YouTube](./documentation/icons/youtube.png "YouTube"){height=16px}](https://www.youtube.com/watch?v=hgF0jNxKrrg)).

### Option 1 - Globally change every WSL distribution

Edit `%USERPROFILE%\.wslconfig`:

```
[wsl2]
kernelCommandLine = "sysctl.vm.max_map_count=262144"
```

Use the ![Docker Whale](./documentation/icons/docker-whale.png "Docker Whale"){height=16px} in the system tray to `Quit Docker Desktop` otherwise it will interfere WSL shutdown with the following Pop-up ![Docker Failure](./documentation/icons/docker-failure.png "Docker Failure"){height=16px} *Docker Desktop - WSL distro terminated abruptly*, which might cause the `.wslconfig` change not being picked up:

```
wsl --shutdown
```

### Option 2 - Specific WSL instance `docker-desktop` only

Modify the `/etc/sysctl.conf` within the `docker-desktop` WSL instance:

```
wsl -d docker-desktop
ls -l /etc/sysctl.conf
vi /etc/sysctl.conf
```

**Tip**: In case of ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash use `winpty wsl -d docker-desktop`.

Add `vm.max_map_count` entry to `/etc/sysctl.conf`:

```
# content of this file will override /etc/sysctl.d/*
vm.max_map_count = 262144
```

After saving this change with `:wq` activate it:

```
sysctl vm.max_map_count
sysctl -p
sysctl -qp
sysctl vm.max_map_count
exit
```

To make sure this settings is still active after WSL has been shutdown:

```
wsl -d docker-desktop
ls -l /etc/wsl.conf
vi /etc/wsl.conf
```

Add `boot` entry to `/etc/wsl.conf`:

```
[boot]
command = "sysctl -qp"

[automount]
 …
```

## Install ELK Stack with Docker Compose

The [![Elastic Stack](./documentation/icons/elastic-stack.svg "Elastic Stack"){height=16px} Elastic Stack](https://www.elastic.co/elastic-stack) is comprised of [![Elasticsearch](./documentation/icons/elasticsearch.svg "Elasticsearch"){height=16px} Elasticsearch](https://www.elastic.co/elasticsearch), [![Kibana](./documentation/icons/kibana.svg "Kibana"){height=16px} Kibana](https://www.elastic.co/kibana), [![Beats](./documentation/icons/beats.svg "Beats"){height=16px} Beats](https://www.elastic.co/beats), and [![Logstash](./documentation/icons/logstash.svg "Logstash"){height=16px} Logstash](https://www.elastic.co/logstash) (also known as the ![Elastic Stack](./documentation/icons/elastic-stack.svg "Elastic Stack"){height=16px} ELK Stack) and [more](https://www.elastic.co/elastic-stack/features).

 * [![Elasticsearch](./documentation/icons/elasticsearch.svg "Elasticsearch"){height=16px} Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current) ([Release Notes](https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html)) is the search and analytics engine that powers the Elastic Stack.

 * [![Kibana](./documentation/icons/kibana.svg "Kibana"){height=16px} Kibana](https://www.elastic.co/guide/en/kibana/current) ([Release Notes](https://www.elastic.co/guide/en/kibana/current/release-notes.html)) is a user interface that lets you visualize your Elasticsearch data and navigate the Elastic Stack.

 * [![Beats](./documentation/icons/beats.svg "Beats"){height=16px} Beats](https://www.elastic.co/guide/en/beats/libbeat/current/beats-reference.html) ([Release Notes](https://www.elastic.co/guide/en/beats/libbeat/current/release-notes.html)) are open source data shippers that you install as agents on your servers to send operational data to Elasticsearch.

 * [![Logstash](./documentation/icons/logstash.svg "Logstash"){height=16px} Logstash](https://www.elastic.co/guide/en/logstash/current/introduction.html) ([Release Notes](https://www.elastic.co/guide/en/logstash/current/releasenotes.html)) is an open source data collection engine with real-time pipelining capabilities. Logstash can dynamically unify data from disparate sources and normalize the data into destinations of your choice. Cleanse and democratize all your data for diverse advanced downstream analytics and visualization use cases.

[Elasticsearch Changes Name](https://www.elastic.co/about/press/elasticsearch-changes-name-to-elastic-to-reflect-wide-adoption-beyond-search) to [![Elastic](./documentation/icons/elastic.png "Elastic"){height=16px} Elastic](https://www.elastic.co) to Reflect Wide Adoption Beyond Search.

### Multi-node cluster with Docker Compose

Use [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} Docker Compose](https://docs.docker.com/compose) to start a [![Elastic](./documentation/icons/elastic.png "Elastic"){height=16px} three-node Elasticsearch cluster](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html) with Kibana (at the time of writing based on `8.11.0`):

 * Use an [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} environment file](https://docs.docker.com/compose/environment-variables/env-file) in Docker Compose ([![GitHub](./documentation/icons/github.png "GitHub"){height=16px} .env](https://github.com/elastic/elasticsearch/blob/main/docs/reference/setup/install/docker/.env)).
 * The [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} Compose file](https://docs.docker.com/compose/compose-file) ([![GitHub](./documentation/icons/github.png "GitHub"){height=16px} docker-compose.yml](https://github.com/elastic/elasticsearch/blob/main/docs/reference/setup/install/docker/docker-compose.yml)).

Apply the following changes:

 * Define the `COMPOSE_PROJECT_NAME` (see also [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} Set or change pre-defined environment variables in Docker Compose](https://docs.docker.com/compose/environment-variables/envvars)).

 * Define passwords for the `elastic` and `kibana_system` users.

 * Set `STACK_VERSION` to the current Elastic Stack version.

 * Avoid exposing port 9200 to external hosts.

 * Also avoid exposing port 5601 to external hosts.

 * Uniform naming of Elasticsearch and Kibana services.

## Run ELK Stack with Docker Compose

Start the `elastic-stack` with:

```
docker compose up -d
```

**Note**: See **Apache Lucene MMapDirectory Tuning** above in case of the following error during startup *max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]*.

Verify the `elastic-stack` status with:

```
docker image ls
docker image ls --all

docker network ls
docker network inspect elastic-stack_default

docker ps
docker ps --format "table {{.Image}}\t{{.Ports}}\t{{.Names}}\t{{.Status}}\t{{.RunningFor}}"

IMAGE                                                  PORTS                                NAMES                   STATUS                            CREATED
docker.elastic.co/kibana/kibana:8.11.0                 127.0.0.1:5601->5601/tcp             elastic-stack-kb01-1    Up 3 seconds (health: starting)   26 hours ago
docker.elastic.co/elasticsearch/elasticsearch:8.11.0   9200/tcp, 9300/tcp                   elastic-stack-es03-1    Up 44 seconds (healthy)           26 hours ago
docker.elastic.co/elasticsearch/elasticsearch:8.11.0   9200/tcp, 9300/tcp                   elastic-stack-es02-1    Up 45 seconds (healthy)           26 hours ago
docker.elastic.co/elasticsearch/elasticsearch:8.11.0   127.0.0.1:9200->9200/tcp, 9300/tcp   elastic-stack-es01-1    Up 45 seconds (healthy)           26 hours ago
docker.elastic.co/elasticsearch/elasticsearch:8.11.0   9200/tcp, 9300/tcp                   elastic-stack-setup-1   Up 47 seconds (healthy)           26 hours ago
```

When `elastic-stack-kb01-1` also has status `Up … seconds (healthy)` access Kibana at [http://localhost:5601](http://localhost:5601) with user `elastic`.

Select ![Default Space](./documentation/icons/kibana-default-space.png "Default Space"){height=16px} > `Manage spaces` > `Advanced Settings` > `Global Settings` > `Share usage with Elastic` > `Usage collection` > `Off` at [http://localhost:5601/app/management/kibana/settings](http://localhost:5601/app/management/kibana/settings).

Stop and start the services with:

```
docker compose stop
docker ps
docker compose start
```

Stop and remove containers and network with:

```
docker container ls
docker container ls --all

docker compose down

docker container ls --all
```

To start completely anew delete the network, containers, and volumes with:

```
docker volume ls

docker compose down -v

docker volume ls
```

**Tip**: To have a similar progress experience in ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash as in ![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS and ![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD prepend `docker compose` command with `winpty`, for example:

```
winpty docker compose up -d
```
